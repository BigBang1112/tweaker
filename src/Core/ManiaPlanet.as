#if MP4
class Vendor
{
    string skyPath;

    CGameScene@ scene;
    CSceneMobil@ skydome;
}

class Mania : Game
{
    void FullSkyDomeToggle(bool enabled)
    {
        if (game.initialised) {
            if (enabled) {
                CSystemFidFile@ fidSkyFull = Fids::GetGame(skyPath + "SkyDomeDouble.Solid.Gbx");
                CPlugSolid@ skyFull = cast<CPlugSolid>(fidSkyFull.Preload());
                @skydome.Solid = skyFull;
            } else {
                CSystemFidFile@ fidSkyHalf = Fids::GetGame(skyPath + "SkyDome.Solid.Gbx");
                CPlugSolid@ skyHalf = cast<CPlugSolid>(fidSkyHalf.Preload());
                @skydome.Solid = skyHalf;
            }
        }
    }

    void VendorGame() override
    {
        skyPath = "GameData/Sky/Media/Solid/";
    }

    void AddVendorNods() override
    {
        string environment = app.RootMap.CollectionName;
        @scene = app.GameScene;
        @camera = cast<CHmsCamera>(app.GameCamera.SceneCamera.HmsPoc);

        for (uint i = 0; i < scene.Mobils.Length; i++) {
            auto mobil = scene.Mobils[i];
            if (environment == "Stadium") {
                if (mobil.IdName == "SkyDome") {
                    @skydome = mobil;
                }
            } else {
                if (i == 1) {
                    @skydome = mobil;
                }
            }
        }
        InitNods();
    }

    void RemoveVendorNods() override
    {
        @scene = null;
    }

    void VendorRoutine() override
    {
        if (initialised) {
            if (camera !is null) {
                camera.ZClipEnable = Setting_ZClip;
                camera.ZClipValue = Setting_ZClipDistance;
                if (Setting_ZClip) {
                    camera.ZClipZBuffer1End = 1.f;
                    camera.ZClipZBuffer2Start = 0;
                }
            }
        }
    }
}
#endif
