# Tweaker <sup><sub>Trackmania ManiaPlanet Turbo</sub></sup>
Modify in-game hidden settings like *Draw Distance* and slightly boost the performance.

## Preview
You can download *Preview* (like *Beta*) version [here](https://openplanet.dev/plugin/tweakerpreview). Keep in mind it **will** conflict with your current version, therefore you must uninstall the old version first. Use at your own risk!

## Features
* Graphics
    * Draw Distance
* Environment
    * Full SkyDome <sup>MP Turbo</sup>
* Interface
    * Overlay Scaling <sup>TM</sup>
    * FPS Counter

## Download
* [Openplanet](https://openplanet.dev/plugin/tweaker)
* [Releases](https://gitlab.com/fentrasLABS/openplanet/tweaker/-/releases)

## Screenshots
![](_git/1.png)
![](_git/2.png)
![](_git/3.png)

## Credits
- *Signature fix* by [XertroV](https://gitlab.com/XertroV/tweaker/-/commit/165cc8f690ba66d97c9aba5a53a40d6dc2f8f957)
- *UI tabs* by [Miss](https://github.com/openplanet-nl/plugin-manager/tree/master/src/Interface)
- *Thumbnail background* by [blackpulse](https://trackmania.exchange/maps/54009)
- *Project icon* by [Fork Awesome](https://forkaweso.me/)